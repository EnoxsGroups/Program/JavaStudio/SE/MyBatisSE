MyBatisUtil
======
封裝的 MyBatis 工具，配置 mybatis-config 設定檔，建立資料庫連線

檔案路徑
------
+ java: src/enoxs/com/util/MyBatisUtil.java
+ test: test/enoxs/com/InitSQLiteDbMapperTest.java
+ doc: doc/enoxs/com/util/MyBatisUtil.md
+ res: 
    + config : res/mybatis-config.xml
    + db_conn : res/db.properties
    + log4j : res/log4j.xml
    + database: 
        + SQLite : res/mybatis.db (default)
        + MS-SQL 
        + MySQL     

使用說明
------
### Example: InitSQLiteDbMapperTest

@BeforeClass
```java
@BeforeClass
public static void beforeClass(){
    MyBatisUtil.start("mybatis-config.xml");
    MyBatisUtil.sqlSession.getMapper(InitSQLiteDbMapper.class);
    initSQLiteDbMapper = MyBatisUtil.sqlSession.getMapper(InitSQLiteDbMapper.class);
}
```
配置 res/mybatis-config.xml 設定，建立連線，取得 DAO (initSQLiteDbMapper)物件

@Test
```java
@Test
public void selectCount(){
    Map<String, String> map = new HashMap<String, String>(){{
        put("tableName", "app_info");
    }};

    Integer count = initSQLiteDbMapper.selectCount(map);
    System.out.println("count : " + count);
}

```
使用 initSQLiteDbMapper 調用 selectCount 接口，執行 select count(*) from app_info 指令

```
DEBUG [main] ==>  Preparing: select count(*) from app_info 
DEBUG [main] ==> Parameters: 
TRACE [main] <==    Columns: count(*)
TRACE [main] <==        Row: 3
DEBUG [main] <==      Total: 1
count : 3
```

@AfterClass
```java
@AfterClass
public static void afterClass(){
    MyBatisUtil.done();
}
```
完成 SQL 查詢後，關閉連線。

依賴：
+ mybatis-3.4.5.jar
+ SQL Driver 
    + sqlite-jdbc-3.23.1.jar (SQLite)
    + sqljdbc4-4.0.jar (MS-SQL)
    + mysql-connector-java-5.1.39-bin.jar (MySQL) 