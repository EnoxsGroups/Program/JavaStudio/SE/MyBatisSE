package enoxs.com.sql.dao;

import enoxs.com.data.gen.model.AppInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface AppInfoDao {

    String insertSQL =
            "insert into app_info(name,version,author,date,remark) values " +
                    "('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3')\n";

    String selectSQL =
            "select * from app_info";

    String selectBy =
            "select id,name,version from app_info where version='1.0.3'";

    String selectByCondition01 =
            "select * from app_info where id > 1"; // > < =

    String selectByCondition02 =
            "select * from app_info where version != '1.0.2'"; // MS-SQL : NOT

    String selectByAND =
            "select * from app_info where version='1.0.3' and author = 'Enoxs'";

    String selectByOR =
            "select * from app_info where version='1.0.3' or author = 'Enoxs'";

    String updateSQL =
            "update app_info " +
            "set name='MyBatisSE', version='1.0.1', author='Enoxs', `date`='2021-08-06', remark='MyBatis - Sample Example' " +
            "where id=4";

    String updateBy =
            "update app_info " +
            "set name='MyBatisSE', version='1.0.2', author='Enoxs', `date`='2021-08-06', remark='MyBatis - Sample Example' " +
            "where name='MyBatisSE' and version = '1.0.1'";

    String deleteSQL =
            "delete from app_info where id=4";


    String selectByLike01 =
            "select * from app_info where name like '%SE'";

    String selectByLike02 =
            "select * from app_info where name like '%SE-%'";

    String selectByLike03 =
            "select * from app_info where name like 'J%'";

    String selectByLike04 =
            "select * from app_info where version like '1.0._'";

    String selectByLike05 =
            "select * from app_info where version like '_.0.2'";

    String selectByLimit =
            "select * from app_info limit 2"; // MS-SQL : TOP 2
    // MS-SQL : select top 2 * from app_info;

    String selectByOrder01 =
            "select * from app_info order by id asc";

    String selectByOrder02 =
            "select * from app_info order by id desc";

    String selectByOrder03 =
            "select * from app_info order by version , id asc";

    String selectByGroup =
            "select version , count(*) from app_info group by version order by version asc";
            // 搭配聚合函式一起使用 : AVG , COUNT , MAX , MIN , SUM
            // 順序 where -> group by -> order by

    String selectByDistinct =
            "select distinct author , version from app_info";


    @Insert(insertSQL)
    void insertAppInfo();

    @Select(selectSQL)
    List<AppInfo> selectSQL();

    @Select(selectBy)
    List<AppInfo> selectWhere();

    @Select(selectByCondition01)
    List<AppInfo> selectWhereCondition();

    @Select(selectByCondition02)
    List<AppInfo> selectWhereNot();

    @Select(selectByAND)
    List<AppInfo> selectWhereAND();

    @Select(selectByOR)
    List<AppInfo> selectWhereOR();

    @Update(updateSQL)
    void updateSQL();

    @Update(updateBy)
    void updateBy();

    @Delete(deleteSQL)
    void deleteSQL();


    // More +
    @Select(selectByLike01)
    List<AppInfo> selectByLike01();

    @Select(selectByLike02)
    List<AppInfo> selectByLike02();

    @Select(selectByLike03)
    List<AppInfo> selectByLike03();

    @Select(selectByLike04)
    List<AppInfo> selectByLike04();

    @Select(selectByLike05)
    List<AppInfo> selectByLike05();

    @Select(selectByLimit)
    List<AppInfo> selectByLimit();

    @Select(selectByOrder01)
    List<AppInfo> setSelectByOrder01();

    @Select(selectByOrder02)
    List<AppInfo> setSelectByOrder02();

    @Select(selectByOrder03)
    List<AppInfo> selectByOrder03();

    @Select(selectByGroup)
    List<AppInfo> SelectByDistinct();

    @Select(selectByGroup)
    List<AppInfo> setSelectByDistinct();
}
