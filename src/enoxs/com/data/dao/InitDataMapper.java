package enoxs.com.data.dao;

public interface InitDataMapper {
    void insertAppInfoData();
    void insertUserInfoData();
    void insertAppGroupData();
    void insertAppSubGroupData();
    void insertAppGroupProjectData();
}
