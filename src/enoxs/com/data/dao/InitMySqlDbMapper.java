package enoxs.com.data.dao;

public interface InitMySqlDbMapper {
    void dropAppInfoTable();
    void dropUserInfoTable();
    void dropAppGroupTable();
    void dropAppSubGroupTable();
    void dropAppGroupProjectTable();

    void createAppInfoTable();
    void createUserInfoTable();
    void createAppGroupTable();
    void createAppSubGroupTable();
    void createAppGroupProjectTable();
}
