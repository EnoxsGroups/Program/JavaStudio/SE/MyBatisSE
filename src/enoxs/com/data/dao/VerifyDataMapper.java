package enoxs.com.data.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VerifyDataMapper {
    /**
     * annotation parameter
     */
    List<Map<String,Object>> selectOneMapResult(@Param(value="id") Long id);

    /**
     * dynamic query table and parameter
     */
    List<Map<String,Object>> selectMapResult(Map<String, String> condition);
    Integer selectCount(Map<String, String> condition);
}
