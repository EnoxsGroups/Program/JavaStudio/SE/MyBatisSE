package enoxs.com.data.dao;

public interface InitMsSqlDbMapper{
    void dropAppInfoTable();
    void dropUserInfoTable();
    void dropAppGroupTable();
    void dropAppSubGroupTable();
    void dropAppGroupProjectTable();

    void createAppInfoTable();
    void createUserInfoTable();
    void createAppGroupTable();
    void createAppSubGroupTable();
    void createAppGroupProjectTable();
}
