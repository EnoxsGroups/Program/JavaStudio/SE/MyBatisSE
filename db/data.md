開發測試資料
======

app_info
------
Field
    
    id,name,version,author,date,remark
+ id=1
    + 'JavaProjSE-v1.0.3',
    + '1.0.3',
    + 'Enoxs',
    + '2019-07-24',
    + 'Java Project Simple Example - Version 1.0.3'
+ id=2
    + 'JUnitSE',
    + '1.0.2',
    + 'Enoxs',
    + '2019-08-17',
    + 'Java Unit Test Simple Example'
+ id=3
    + 'SpringMVC-SE',
    + '1.0.2',
    + 'Enoxs',
    + '2019-07-31',
    + 'Java Web Application Spring MVC 框架 - Simple Example 未整合持久化框架（MyBatis）'
    
user_info
------
Field

    id,name,account,password,remark
+ id=1
    + '夢想架構師',
    + 'Enoxs',
    + '00000000',
    + '專注於 Java / Android，架構夢想的軟體開發工程師'
+ id=2
    + '系統管理人員',
    + 'admin',
    + 'root0000',
    + '最高管理權限，系統人員'
+ id=3
    + '測試人員',
    + 'Test',
    + '0000',
    + '一般操作權限，使用者' 

app_group
------
Field
    
    id,name,description
+ id=1
    + Program
    + 程式範例
+ id=2
    + JavaStudio
    + Java 程式範例
+ id=3 
    + SE
    + Java 程式範例 SE 系列
    
app_sub_group
------
Field

    group_id,sub_group_id
+ 1,2
+ 2,3

app_group_project
------
Field

    group_id,app_id
+ 3,1
+ 3,2
+ 3,3