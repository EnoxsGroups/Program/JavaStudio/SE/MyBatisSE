開發測試資料表
======

app_info
------
程式專案資訊

| name | type | spec |
| ---- | ---- | ---- |
|id     | long | key && auto |
|name   | text | 50 |
|version| text | 30 |
|author | text | 50|
|date   | datetime | &nbsp;|
|remark | text | 100 |

    author mapping -> user_info.account  

user_info
------
使用者資訊

| name | type | spec |
| ---- | ---- | ---- |
|id       | long | key && auto |
|name     | text | 50 |
|account  | text | 50 |
|password | text | 50|
|remark   | text | 100 |

app_group
------
程式群組資訊

| name | type | spec |
| ---- | ---- | ---- |
|id       | long | key && auto |
|name     | text | 50 |
|description   | text | 100 |

app_sub_group
------
程式子群組對應表

| name | type | spec |
| ---- | ---- | ---- |
| group_id     | long | &nbsp; |
| sub_group_id | long | &nbsp; |

    join app_group
    子群組

app_group_project
------
程式群組與專案對應表

| name | type | spec |
| ---- | ---- | ---- |
| group_id | long | &nbsp; |
| app_id   | long | &nbsp; |

    join app_group 
    join app_info
    群組下的專案
