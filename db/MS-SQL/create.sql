-- app_info
drop table if exists app_info
create table [dbo].[app_info](
	[id] [bigint] identity(1,1) NOT NULL,
	[name] nvarchar(50),
	[version] nvarchar(30),
	[author] nvarchar(50) ,
	[date] datetime,
	[remark] nvarchar(100)
primary key CLUSTERED ([id] ASC)
) on [primary]

-- user_info

drop table if exists user_info
create table [dbo].[user_info](
	[id] [bigint] identity(1,1) NOT NULL,
	[name] nvarchar(50) NOT NULL,
	[account] nvarchar(50) NOT NULL,
	[password] nvarchar(50) NOT NULL,
	[remark] nvarchar(100)
primary key CLUSTERED ([id] ASC)
) on [primary]

-- app_group

drop table if exists app_group
create table [dbo].[app_group](
	[id] [bigint] identity(1,1) NOT NULL,
	[name] nvarchar(50),
	[description] nvarchar(100)
primary key CLUSTERED ([id] ASC)
) on [primary]

-- app_sub_group

drop table if exists app_sub_group
create table [dbo].[app_sub_group](
	[group_id] [bigint] NOT NULL,
	[sub_group_id] [bigint]  NOT NULL
)


-- app_group_project
drop table if exists app_group_project
create table [dbo].[app_group_project](
	[group_id] [bigint] NOT NULL,
	[app_id] [bigint]  NOT NULL
)


