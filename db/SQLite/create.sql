-- app_info
create table app_info (
	'id'	integer primary key autoincrement,
	'name'	text,
	'version'	text,
	'author'	text,
	'date'	text,
	'remark'	text
);

-- user_info
create table user_info (
	'id' integer primary key autoincrement,
	'name' text,
	'account' text,
	'password' text,
	'remark' text
);

-- app_group
create table app_group (
	'id' integer primary key autoincrement,
	'name' text,
	'description' text
);

-- app_sub_group
create table app_sub_group (
	'group_id' integer,
	'sub_group_id' integer
);

-- app_group_project
create table app_group_project (
	'group_id' integer,
	'app_id' integer
);

