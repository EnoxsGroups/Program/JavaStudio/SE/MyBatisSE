Java MyBatis Simple Example
======

![cover](assets/readme/cover.png)

專案說明
------
Java MyBatis 持久化框架 - Simple Example 

連線 SQL 驗證測試，已配置 MS-SQL、MySQL、SQLite 連線設定

+ 整併 Spring + MyBatis 企業應用實戰（第2版）練習範例：
    + <https://github.com/RHZEnoxs/JavaMybatisSE-v1.0.1>

模組說明
------

### Java 主程式
dir: src/enoxs/com/
+ data/ 
        Mybatis 持久化物件
    + dao/
            客製化接口，獨立或繼承 gen dao
    + mapper/
            客製化對應，獨立或繼承 gen mapper
    + gen/
            mybatis-generator 工具產生物件
        + dao/
        + mapper/
        + model/
+ util/
    + MyBatisUtil
    
            封裝 MyBatis 工具

### 單元測試 (test)
dir: test/enoxs/com/
+ dao/
    + InitSQLiteDbMapperTest.java
    
            初始化 SQLite 資料庫資料

        
### 使用手冊 (doc)
dir: doc/enoxs/com/
+ util/
    + [MyBatisUtil](doc/enoxs/com/util/MyBatisUtil.md)


操作環境
------
+ IDE : IntelliJ
+ SDK : OpenJDK8U

專案設定 - 模組
------
### Project Settings > Modules
![Modules](assets/readme/modules.png)

+ res/
    + 配置檔
+ test/ 
    + 單元測試

