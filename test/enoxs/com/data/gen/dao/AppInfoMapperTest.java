package enoxs.com.data.gen.dao;

import enoxs.com.data.gen.model.AppInfo;
import enoxs.com.util.MyBatisUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AppInfoMapperTest {

    AppInfoMapper appInfoMapper = null;

    @Before
    public void setUp() throws Exception {
        MyBatisUtil.start("mybatis-config.xml");
        MyBatisUtil.sqlSession.getMapper(AppInfoMapper.class);
        appInfoMapper = MyBatisUtil.sqlSession.getMapper(AppInfoMapper.class);
    }

    @After
    public void tearDown() throws Exception {
        MyBatisUtil.done();
    }


    @Test
    public void deleteByPrimaryKey() {
        List<AppInfo> lstAppInfo = appInfoMapper.selectAll();
        Integer delId = lstAppInfo.get(lstAppInfo.size()-1).getId();
        appInfoMapper.deleteByPrimaryKey(delId);
    }

    @Test
    public void insert() {
        AppInfo appInfo = new AppInfo();
        appInfo.setName("MyBatisSE");
        appInfo.setVersion("1.0.1");
        appInfo.setAuthor("Enoxs");
        appInfo.setDate(new Date());
        appInfo.setRemark("MyBatis - Sample Example");
        appInfoMapper.insert(appInfo);
        System.out.println(appInfo.getId());
    }

    @Test
    public void selectByPrimaryKey() {
        Integer id = 1;
        AppInfo appInfo = appInfoMapper.selectByPrimaryKey(id);
    }

    @Test
    public void selectAll() {
        List<AppInfo> lstAppInfo = appInfoMapper.selectAll();
        for(AppInfo info : lstAppInfo){
            System.out.println("id : " + info.getId());
            System.out.println("name : " + info.getName());
            System.out.println("version : " + info.getVersion());
            System.out.println("author : " + info.getAuthor());
            System.out.println("date : " + info.getDate());
            System.out.println("remark : " + info.getRemark());
        }
    }

    @Test
    public void updateByPrimaryKey() {
        List<AppInfo> lstAppInfo = appInfoMapper.selectAll();
        AppInfo appInfo = lstAppInfo.get(lstAppInfo.size()-1);
        appInfo.setName("MyBatisSE");
        appInfo.setVersion("1.0.1");
        appInfo.setAuthor("Enoxs");
        appInfo.setDate(new Date());
        appInfo.setRemark("MyBatis - Sample Example");
        appInfoMapper.updateByPrimaryKey(appInfo);
    }

}