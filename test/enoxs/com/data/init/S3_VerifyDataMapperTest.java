package enoxs.com.data.init;

import enoxs.com.data.dao.VerifyDataMapper;
import enoxs.com.util.MyBatisUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class S3_VerifyDataMapperTest {
    protected static VerifyDataMapper verifyDataMapper = null;


    @BeforeClass
    public static void beforeClass(){
        MyBatisUtil.start("mybatis-config.xml");
        verifyDataMapper = MyBatisUtil.sqlSession.getMapper(VerifyDataMapper.class);
    }

    @AfterClass
    public static void afterClass(){
        MyBatisUtil.done();
    }
    @Test
    public void selectOneMapResult(){
        Long id = Long.valueOf(1);
        verifyDataMapper.selectOneMapResult(id);
    }

    @Test
    public void selectMapResult() {
        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
            put("id", "1");
        }};
        verifyDataMapper.selectMapResult(map);
    }

    @Test
    public void selectCount(){
        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
        }};

        Integer count = verifyDataMapper.selectCount(map);
        System.out.println("count : " + count);
    }
}