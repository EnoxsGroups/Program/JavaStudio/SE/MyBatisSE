package enoxs.com.data.init;

import enoxs.com.data.dao.InitMsSqlDbMapper;
import enoxs.com.data.dao.InitSQLiteDbMapper;
import enoxs.com.util.MyBatisUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public abstract class MapperTest {
    protected static InitSQLiteDbMapper initSQLiteDbMapper = null;
    protected static InitMsSqlDbMapper initMsSqlDbMapper = null;
    @BeforeClass
    public static void beforeClass(){
        MyBatisUtil.start("mybatis-config.xml");
        System.out.println("Mybatis Connect Db Status : " + MyBatisUtil.getEnvironmentStatus());
        switch (MyBatisUtil.getEnvironmentStatus()){
            case "mssql":
                initMsSqlDbMapper = MyBatisUtil.sqlSession.getMapper(InitMsSqlDbMapper.class);
                break;
            case "mysql":
                break;
            case "sqlite":
                initSQLiteDbMapper = MyBatisUtil.sqlSession.getMapper(InitSQLiteDbMapper.class);
                break;
        }
    }

    @AfterClass
    public static void afterClass(){
        MyBatisUtil.done();
    }
}
