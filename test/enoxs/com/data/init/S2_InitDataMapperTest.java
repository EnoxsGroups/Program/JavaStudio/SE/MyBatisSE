package enoxs.com.data.init;

import enoxs.com.data.dao.InitDataMapper;
import enoxs.com.data.dao.VerifyDataMapper;
import enoxs.com.util.MyBatisUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class S2_InitDataMapperTest {
    protected static InitDataMapper initDataMapper = null;
    protected static VerifyDataMapper verifyDataMapper = null;
    @BeforeClass
    public static void beforeClass(){
        MyBatisUtil.start("mybatis-config.xml");
        initDataMapper = MyBatisUtil.sqlSession.getMapper(InitDataMapper.class);
        verifyDataMapper = MyBatisUtil.sqlSession.getMapper(VerifyDataMapper.class);
    }

    @AfterClass
    public static void afterClass(){
        MyBatisUtil.done();
    }

    @Test
    public void insertAllData(){
        insertAppInfoData();
        insertUserInfoData();
        insertAppGroupData();
        insertAppSubGroupData();
        insertAppGroupProjectData();
    }

    @Test
    public void insertAppInfoData(){
        initDataMapper.insertAppInfoData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_info");
        }};
        Integer count = verifyDataMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertUserInfoData(){
        initDataMapper.insertUserInfoData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "user_info");
        }};
        Integer count = verifyDataMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppGroupData(){
        initDataMapper.insertAppGroupData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_group");
        }};
        Integer count = verifyDataMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppSubGroupData(){
        initDataMapper.insertAppSubGroupData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_sub_group");
        }};
        Integer count = verifyDataMapper.selectCount(map);
        assertTrue(count > 0);
    }

    @Test
    public void insertAppGroupProjectData(){
        initDataMapper.insertAppGroupProjectData();

        Map<String, String> map = new HashMap<String, String>(){{
            put("tableName", "app_group_project");
        }};
        Integer count = verifyDataMapper.selectCount(map);
        assertTrue(count > 0);
    }
}