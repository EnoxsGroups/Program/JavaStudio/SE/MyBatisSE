package enoxs.com.data.init;

import enoxs.com.data.dao.InitMsSqlDbMapper;
import enoxs.com.data.dao.InitSQLiteDbMapper;
import enoxs.com.util.MyBatisUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class S1_InitDbTableMapperTest extends MapperTest{
    @Test
    public void dropAllTable(){
        switch (MyBatisUtil.getEnvironmentStatus()){
            case "mssql":
                initMsSqlDbMapper.dropAppInfoTable();
                initMsSqlDbMapper.dropUserInfoTable();
                initMsSqlDbMapper.dropAppGroupTable();
                initMsSqlDbMapper.dropAppSubGroupTable();
                initMsSqlDbMapper.dropAppGroupProjectTable();
                break;
            case "mysql":
                break;
            case "sqlite":
                initSQLiteDbMapper.dropAppInfoTable();
                initSQLiteDbMapper.dropUserInfoTable();
                initSQLiteDbMapper.dropAppGroupTable();
                initSQLiteDbMapper.dropAppSubGroupTable();
                initSQLiteDbMapper.dropAppGroupProjectTable();
                break;
        }
    }

    @Test
    public void createAllTable() {
        switch (MyBatisUtil.getEnvironmentStatus()){
            case "mssql":
                initMsSqlDbMapper.createAppInfoTable();
                initMsSqlDbMapper.createUserInfoTable();
                initMsSqlDbMapper.createAppGroupTable();
                initMsSqlDbMapper.createAppSubGroupTable();
                initMsSqlDbMapper.createAppGroupProjectTable();
                break;
            case "mysql":
                break;
            case "sqlite":
                initSQLiteDbMapper.createAppInfoTable();
                initSQLiteDbMapper.createUserInfoTable();
                initSQLiteDbMapper.createAppGroupTable();
                initSQLiteDbMapper.createAppSubGroupTable();
                initSQLiteDbMapper.createAppGroupProjectTable();
                break;
        }
    }

    @Test
    public void dropAppInfoTable(){
        initSQLiteDbMapper.dropAppInfoTable();
    }

    @Test
    public void dropUserInfoTable(){
        initSQLiteDbMapper.dropUserInfoTable();
    }

    @Test
    public void dropAppGroupTable(){
        initSQLiteDbMapper.dropAppGroupTable();
    }

    @Test
    public void dropAppSubGroupTable(){
        initSQLiteDbMapper.dropAppSubGroupTable();
    }

    @Test
    public void dropAppGroupProjectTable(){
        initSQLiteDbMapper.dropAppGroupProjectTable();
    }

    @Test
    public void createAppInfoTable(){
        initSQLiteDbMapper.createAppInfoTable();
    }

    @Test
    public void createUserInfoTable(){
        initSQLiteDbMapper.createUserInfoTable();
    }

    @Test
    public void createAppGroupTable(){
        initSQLiteDbMapper.createAppGroupTable();
    }

    @Test
    public void createAppSubGroupTable(){
        initSQLiteDbMapper.createAppSubGroupTable();
    }

    @Test
    public void createAppGroupProjectTable(){
        initSQLiteDbMapper.createAppGroupProjectTable();
    }
}