package enoxs.com.sql.dao;

import enoxs.com.data.gen.dao.AppInfoMapper;
import enoxs.com.data.gen.model.AppInfo;
import enoxs.com.util.MyBatisUtil;
import junit.framework.TestCase;
import org.apache.ibatis.annotations.Select;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class AppInfoDaoTest{
    AppInfoDao appInfoDao = null;

    @Before
    public void setUp() throws Exception {
        MyBatisUtil.start("mybatis-config.xml");
        MyBatisUtil.sqlSession.getMapper(AppInfoMapper.class);
        appInfoDao = MyBatisUtil.sqlSession.getMapper(AppInfoDao.class);
    }

    @After
    public void tearDown() throws Exception {
        MyBatisUtil.done();
    }

    @Test
    public void insert(){
        appInfoDao.insertAppInfo();
    }

    @Test
    public void select() {
        appInfoDao.selectSQL();
    }


    @Test
    public void selectWhere(){
        appInfoDao.selectWhere();
    }

    @Test
    public void selectWhereCondiftion(){
        appInfoDao.selectWhereCondition();
    }

    @Test
    public void selectWhereNot(){
        appInfoDao.selectWhereNot();
    }

    @Test
    public void selectWhereAND(){
        appInfoDao.selectWhereAND();
    }

    @Test
    public void selectWhereOR(){
        appInfoDao.selectWhereOR();
    }

    @Test
    public void update(){
        appInfoDao.updateSQL();
    }

    @Test
    public void updateWhere(){
        appInfoDao.updateBy();
    }

    @Test
    public void delete(){
        appInfoDao.deleteSQL();
    }



    // More +
    @Test
    public void selectByLike01(){
        appInfoDao.selectByLike01();
    }

    @Test
    public void selectByLike02(){
        appInfoDao.selectByLike02();
    }

    @Test
    public void selectByLike03(){
        appInfoDao.selectByLike03();
    }

    @Test
    public void selectByLike04(){
        appInfoDao.selectByLike04();
    }

    @Test
    public void selectByLike05(){
        appInfoDao.selectByLike05();
    }

    @Test
    public void selectByLimit(){
        appInfoDao.selectByLimit();
    }

    @Test
    public void setSelectByOrder01(){
        appInfoDao.setSelectByOrder01();
    }

    @Test
    public void setSelectByOrder02(){
        appInfoDao.setSelectByOrder02();
    }

    @Test
    public void selectByOrder03(){
        appInfoDao.selectByOrder03();
    }

    @Test
    public void SelectByDistinct(){
        appInfoDao.SelectByDistinct();
    }

    @Test
    public void setSelectByDistinct(){
        appInfoDao.setSelectByDistinct();
    }

}